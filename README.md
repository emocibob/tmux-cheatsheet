# Tmux Cheatsheet

## NB

- you can think of windows as tabs and panes as window splits (multiple visible subwindows)
- the custom config file is called `.tmux.conf` (home dir) 

## Sessions

- `tmux new -s sess` - new session with name `sess`
- `tmux ls` - list sessions
- `Ctrl`+`b`, `d` - deatach from session
- `tmux a -t sess` - attach `sess` session
- `tmux kill-session -t sess` - kill `sess`
- `tmux kill-server` - kill all sessions

## Panes

- `Ctrl`+`b`, `%` - vertical (left-and-right) split
- `Ctrl`+`b`, `"` - horizontal (top-and-bottom) split
- `Ctrl`+`b`, `x` - kill pane
- `Ctrl`+`b`, `q` - show pane ids and then select one to go there
- `Ctrl`+`b`, `o` - go to next pane
- `Ctrl`+`b`, `→` - go to right pane (also: `←`, `↑`, `↓`)
- `Ctrl`+`b`, `;` - toggle between current and previous pane
- `Ctrl`+`b`, `z` - zoom (maximize) in/out on pane
- `Ctrl`+`b`, `t` - show clock in current pane

## Windows

- `Ctrl`+`b`, `c` - new window
- `Ctrl`+`b`, `,` - rename current window
- `Ctrl`+`b`, `0` - go to window `0` (also: `1`, `2`, etc.)
- `Ctrl`+`b`, `w` - list windows
- `Ctrl`+`b`, `&` - kill current window

## Copy/paste

- `Ctrl`+`b`, `[` - copy mode (default: emacs keys)
- `Ctrl`+`b`, `]` - paste

## Other

- `Ctrl`+`b`, `?` - list all commands